package lv.bol.kalaha.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BoardTest {

    @Test
    public void shouldCreateNewBoard() {
        Board board = new Board();

        assertThat(board).isNotNull();
        assertThat(board.getPitList()).isNotNull();
        assertThat(board.getPitList().size()).isEqualTo(14);
    }
}