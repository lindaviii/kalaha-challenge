package lv.bol.kalaha.domain.pits;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SmallPitTest {

    private SmallPit pit;
    private String ID = "sample id";

    @Before
    public void setUp() {
        pit = new SmallPit(ID);
    }

    @Test
    public void shouldAddStones() {
        int initialStoneCount = pit.getNumberOfStones();

        pit.addStones(1);

        assertThat(initialStoneCount).isEqualTo(6);
        assertThat(pit.getId()).isEqualTo(ID);
        assertThat(pit.getNumberOfStones()).isEqualTo(initialStoneCount+1);
    }

    @Test
    public void takeStones() {
        int initialStoneCount = pit.getNumberOfStones();

        pit.takeStones();

        assertThat(initialStoneCount).isEqualTo(6);
        assertThat(pit.getNumberOfStones()).isEqualTo(0);
    }
}