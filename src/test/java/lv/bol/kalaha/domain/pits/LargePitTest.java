package lv.bol.kalaha.domain.pits;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LargePitTest {

    private LargePit pit;
    private String ID = "sample id";

    @Before
    public void setUp() {
        pit = new LargePit(ID);
    }

    @Test
    public void shouldAddStones() {
        int initialStoneCount = pit.getNumberOfStones();

        pit.addStones(1);

        assertThat(initialStoneCount).isEqualTo(0);
        assertThat(pit.getId()).isEqualTo(ID);
        assertThat(pit.getNumberOfStones()).isEqualTo(1);
    }

    @Test
    public void shouldTakeStones() {
        pit.setNumberOfStones(22);

        pit.takeStones();

        assertThat(pit.getNumberOfStones()).isEqualTo(0);
    }
}