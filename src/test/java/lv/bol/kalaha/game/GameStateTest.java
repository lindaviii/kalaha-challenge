package lv.bol.kalaha.game;

import lv.bol.kalaha.domain.Board;
import lv.bol.kalaha.domain.Game;
import lv.bol.kalaha.domain.Player;
import lv.bol.kalaha.domain.pits.SmallPit;
import lv.bol.kalaha.helpers.BoardHelper;
import org.junit.Before;
import org.junit.Test;

import static lv.bol.kalaha.domain.Game.PLAYER_ONE_ID;
import static lv.bol.kalaha.domain.Game.PLAYER_TWO_ID;
import static lv.bol.kalaha.helpers.BoardHelper.TIED_RESULT;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class GameStateTest {

    private String PLAYER_ONE_NAME = "Bob";
    private String PLAYER_TWO_NAME = "Sue";

    private GameState gameState;
    private Game game;

    @Before
    public void setUp() {
        gameState = new GameState();
        game = new Game(PLAYER_ONE_NAME, PLAYER_TWO_NAME);
        gameState.setGame(game);
        gameState.setActivePlayer(game.getPlayerOne());
    }

    @Test
    public void startNewGame() {
        Game initialGame = gameState.getGame();

        gameState.startNewGame("name one", "name two");

        assertThat(initialGame).isNotNull();
        assertThat(gameState.getGame()).isNotNull();
        assertThat(initialGame).isNotEqualTo(gameState.getGame());
    }

    @Test
    public void shouldTakeTurn_Regular() {
        String pitId = "player1-sp4";
        Player initialPlayer = gameState.getActivePlayer();

        gameState.takeTurn(pitId);

        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), pitId).getNumberOfStones()).isEqualTo(0);
        assertThat(initialPlayer.getId()).isEqualTo(PLAYER_ONE_ID);
        assertThat(gameState.getActivePlayer().getId()).isEqualTo(PLAYER_TWO_ID);
    }

    @Test
    public void shouldTakeTurn_AndFinishInOwnLargePit() {
        String pitId = "player1-sp1";
        Player initialPlayer = gameState.getActivePlayer();
        gameState.takeTurn(pitId);

        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), pitId).getNumberOfStones()).isEqualTo(0);
        assertThat(initialPlayer.getId()).isEqualTo(PLAYER_ONE_ID);
        assertThat(gameState.getActivePlayer().getId()).isEqualTo(PLAYER_ONE_ID);
    }

    @Test
    public void shouldTakeTurn_AndNotAddToOponentsLargePit() {
        String pitId = "player1-sp6";
        BoardHelper.getPitById(gameState.getGame().getBoard(), pitId).setNumberOfStones(10);
        Player initialPlayer = gameState.getActivePlayer();

        gameState.takeTurn(pitId);

        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), pitId).getNumberOfStones()).isEqualTo(0);
        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), Board.PLAYER_TWO_LARGE_PIT).getNumberOfStones()).isEqualTo(0);
        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), Board.PLAYER_ONE_LARGE_PIT).getNumberOfStones()).isEqualTo(1);
        assertThat(initialPlayer.getId()).isEqualTo(PLAYER_ONE_ID);
        assertThat(gameState.getActivePlayer().getId()).isEqualTo(PLAYER_TWO_ID);
    }

    @Test
    public void shouldTakeTurn_AndCaptureOponentsStones() {
        String pitId1 = "player1-sp4";
        BoardHelper.getPitById(gameState.getGame().getBoard(), pitId1).setNumberOfStones(1);
        String pitId2 = "player1-sp5";
        BoardHelper.getPitById(gameState.getGame().getBoard(), pitId2).setNumberOfStones(0);
        String pitId2Opposing = "player2-sp5";
        BoardHelper.getPitById(gameState.getGame().getBoard(), pitId2Opposing).setNumberOfStones(22);
        Player initialPlayer = gameState.getActivePlayer();

        gameState.takeTurn(pitId1);

        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), pitId1).getNumberOfStones()).isEqualTo(0);
        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), pitId2).getNumberOfStones()).isEqualTo(23);
        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), pitId2Opposing).getNumberOfStones()).isEqualTo(0);
        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), Board.PLAYER_TWO_LARGE_PIT).getNumberOfStones()).isEqualTo(0);
        assertThat(BoardHelper.getPitById(gameState.getGame().getBoard(), Board.PLAYER_ONE_LARGE_PIT).getNumberOfStones()).isEqualTo(0);
        assertThat(initialPlayer.getId()).isEqualTo(PLAYER_ONE_ID);
        assertThat(gameState.getActivePlayer().getId()).isEqualTo(PLAYER_TWO_ID);
    }

    @Test
    public void shouldChangeActivePlayer() {
        Player defaultActivePlayer = gameState.getActivePlayer();

        gameState.changeActivePlayer();
        Player newActivePlayer = gameState.getActivePlayer();

        assertThat(defaultActivePlayer).isNotNull();
        assertThat(defaultActivePlayer.getId()).isEqualTo(PLAYER_ONE_ID);
        assertThat(defaultActivePlayer.getName()).isEqualTo(PLAYER_ONE_NAME);
        assertThat(newActivePlayer).isNotNull();
        assertThat(newActivePlayer).isNotEqualTo(defaultActivePlayer);
        assertThat(newActivePlayer.getId()).isEqualTo(PLAYER_TWO_ID);
        assertThat(newActivePlayer.getName()).isEqualTo(PLAYER_TWO_NAME);
    }

    @Test
    public void shouldCheckIfGameOver_WithInitialSetup() {
        boolean result = gameState.isGameOver();

        assertThat(result).isFalse();
    }

    @Test
    public void getBoard() {
        Board board = gameState.getGame().getBoard();

        assertThat(board).isNotNull();
    }

    @Test
    public void shouldGetWinnerName() {
        String result = gameState.getWinnerName();

        assertThat(result).isEqualTo(TIED_RESULT);
    }
}