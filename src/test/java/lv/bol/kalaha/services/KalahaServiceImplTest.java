package lv.bol.kalaha.services;

import lv.bol.kalaha.domain.Board;
import lv.bol.kalaha.domain.Game;
import lv.bol.kalaha.domain.Player;
import lv.bol.kalaha.game.GameState;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static lv.bol.kalaha.domain.Board.PLAYER_ONE_LARGE_PIT;
import static lv.bol.kalaha.domain.Board.PLAYER_TWO_LARGE_PIT;
import static lv.bol.kalaha.domain.Game.PLAYER_ONE_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class KalahaServiceImplTest {

    private KalahaService kalahaService;
    @Mock
    private GameState gameState;
    @Mock
    private Game game;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        kalahaService = new KalahaServiceImpl(gameState);
    }

    @Test
    public void shouldCheckIfGameOver() {
        given(gameState.isGameOver()).willReturn(false);

        boolean result = kalahaService.isGameOver();

        assertThat(result).isFalse();
    }

    @Test
    public void shouldGetWinnerName() {
        String winner = "winner";
        given(gameState.getWinnerName()).willReturn(winner);

        String result = kalahaService.getWinnerName();

        assertThat(result).isEqualTo(winner);
    }

    @Test
    public void takeTurn() {
        String pitId = "sample pit id";

        kalahaService.takeTurn(pitId);

        verify(gameState, times(1)).takeTurn(pitId);
    }

    @Test
    public void startNewGame() {
        String playerOneName = "player 1";
        String playerTwoName = "player 2";
        kalahaService.startNewGame(playerOneName, playerTwoName);

        verify(gameState, times(1)).startNewGame(playerOneName, playerTwoName);
    }

    @Test
    public void getActivePlayerName() {
        String name = "John";
        Player player = new Player("p1", name);
        given(gameState.getActivePlayer()).willReturn(player);

        String result = kalahaService.getActivePlayerName();

        assertThat(result).isEqualTo(name);
    }

    @Test
    public void firstPlayerOptionsAvailable() {
        Player player = new Player(PLAYER_ONE_ID, "John");
        given(gameState.getActivePlayer()).willReturn(player);

        boolean result = kalahaService.firstPlayerOptionsAvailable();

        assertThat(result).isTrue();
    }

    @Test
    public void getStoneCounts() {
        given(gameState.getGame()).willReturn(game);
        given(game.getBoard()).willReturn(new Board());

        Map<String, Integer> result = kalahaService.getStoneCounts();

        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result.size()).isEqualTo(14);
        assertThat(result.get("player1-sp4")).isEqualTo(6);
        assertThat(result.get("player2-sp6")).isEqualTo(6);
        assertThat(result.get(PLAYER_ONE_LARGE_PIT)).isEqualTo(0);
        assertThat(result.get(PLAYER_TWO_LARGE_PIT)).isEqualTo(0);
    }
}