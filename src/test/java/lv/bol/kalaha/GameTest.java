package lv.bol.kalaha;

import lv.bol.kalaha.domain.Game;
import org.junit.Test;

import static lv.bol.kalaha.domain.Game.PLAYER_ONE_ID;
import static lv.bol.kalaha.domain.Game.PLAYER_TWO_ID;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class GameTest {

    @Test
    public void newGameCreated() {
        String playerOneName = "Bob";
        String playerTwoName = "Sue";

        Game game = new Game(playerOneName, playerTwoName);

        assertThat(game).isNotNull();
        assertThat(game.getPlayerOne()).isNotNull();
        assertThat(game.getPlayerOne().getName()).isEqualTo(playerOneName);
        assertThat(game.getPlayerOne().getId()).isEqualTo(PLAYER_ONE_ID);
        assertThat(game.getPlayerTwo()).isNotNull();
        assertThat(game.getPlayerTwo().getName()).isEqualTo(playerTwoName);
        assertThat(game.getPlayerTwo().getId()).isEqualTo(PLAYER_TWO_ID);
        assertThat(game.getBoard()).isNotNull();
    }
}