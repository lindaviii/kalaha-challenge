package lv.bol.kalaha;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import lv.bol.kalaha.domain.pits.LargePit;
import lv.bol.kalaha.domain.pits.Pit;
import lv.bol.kalaha.domain.pits.SmallPit;
import lv.bol.kalaha.domain.Board;
import lv.bol.kalaha.helpers.BoardHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static lv.bol.kalaha.domain.Board.PLAYER_ONE_LARGE_PIT;
import static lv.bol.kalaha.domain.Board.PLAYER_TWO_LARGE_PIT;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class BoardHelperTest {

    private Board board;

    @Before
    public void setUp() {
        board = new Board();
    }

    @Test
    @Parameters({"17, 0, true",
            "22, 5, false" })
    public void shouldCheckIfAllSmallPitsEmptyForAnyPlayer(int firstStoneCount, int secondStoneCount, boolean expected) {
        BoardHelper.getPitById(board, "player1-sp1").setNumberOfStones(firstStoneCount);
        BoardHelper.getPitById(board, "player2-sp1").setNumberOfStones(secondStoneCount);
        BoardHelper.getPitById(board, "player2-sp2").setNumberOfStones(secondStoneCount);
        BoardHelper.getPitById(board, "player2-sp3").setNumberOfStones(secondStoneCount);
        BoardHelper.getPitById(board, "player2-sp4").setNumberOfStones(secondStoneCount);
        BoardHelper.getPitById(board, "player2-sp5").setNumberOfStones(secondStoneCount);
        BoardHelper.getPitById(board, "player2-sp6").setNumberOfStones(secondStoneCount);

        boolean result = BoardHelper.allSmallPitsEmptyForAnyPlayer(board);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    @Parameters({"17, 10, player1",
            "5, 22, player2",
            "5, 5, Friendship! It's a tie"})
    public void getWinner(int firstStoneCount, int secondStoneCount, String expected) {
        BoardHelper.getPitById(board, PLAYER_ONE_LARGE_PIT).setNumberOfStones(firstStoneCount);
        BoardHelper.getPitById(board, PLAYER_TWO_LARGE_PIT).setNumberOfStones(secondStoneCount);

        String result = BoardHelper.getWinnerName(board);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldGetNextPit() {
        Pit pit = BoardHelper.getPitByIndex(board,0);
        Pit nextPit = BoardHelper.getNextPit(board, pit);

        assertThat(nextPit).isNotNull();
        assertThat(nextPit.getId()).isEqualTo("player1-sp2");
    }

    @Test
    public void shouldGetNextPit_EndOfList() {
        Pit pit = BoardHelper.getPitByIndex(board, 13);
        String startingPitId = pit.getId();

        Pit nextPit = BoardHelper.getNextPit(board, pit);

        assertThat(nextPit).isNotNull();
        assertThat(nextPit.getId()).isEqualTo("player1-sp1");
        assertThat(startingPitId).isEqualTo("player2-lp");
    }

    @Test
    public void shouldGetStoneCountByPitId() {
        String pitId = "player1-sp1";
        int expected = 19;
        BoardHelper.getPitById(board, pitId).setNumberOfStones(expected);

        int result = BoardHelper.getStoneCountByPitId(board, pitId);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldGetOppositePit() {
        Pit startingPit = BoardHelper.getPitById(board, "player2-sp2");

        Pit oppositePit = BoardHelper.getOppositePit(board, startingPit);

        assertThat(oppositePit).isNotNull();
        assertThat(oppositePit.getId()).isEqualTo("player1-sp2");
    }
}