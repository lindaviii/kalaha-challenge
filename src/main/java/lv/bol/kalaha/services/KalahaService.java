package lv.bol.kalaha.services;

import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface KalahaService {

    boolean isGameOver();
    String getWinnerName();
    void takeTurn(String pitId);
    void startNewGame(String p1Name, String p2Name);
    String getActivePlayerName();
    boolean firstPlayerOptionsAvailable();
    Map<String, Integer> getStoneCounts();
}