package lv.bol.kalaha.services;

import lombok.RequiredArgsConstructor;
import lv.bol.kalaha.domain.pits.Pit;
import lv.bol.kalaha.game.GameState;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

import static lv.bol.kalaha.domain.Game.PLAYER_ONE_ID;

@RequiredArgsConstructor
@Component
@Primary
public class KalahaServiceImpl implements KalahaService {

    private final GameState gameState;

    @Override
    public boolean isGameOver() {
        return gameState.isGameOver();
    }

    @Override
    public String getWinnerName() {
        return gameState.getWinnerName();
    }

    @Override
    public void takeTurn(String pitId) {
        gameState.takeTurn(pitId);
    }

    @Override
    public void startNewGame(String playerOneName, String playerTwoName) {
        gameState.startNewGame(playerOneName, playerTwoName);
    }

    @Override
    public String getActivePlayerName() {
        return gameState.getActivePlayer().getName();
    }

    @Override
    public boolean firstPlayerOptionsAvailable() {
        return gameState.getActivePlayer().getId().equals(PLAYER_ONE_ID);
    }

    @Override
    public Map<String, Integer> getStoneCounts() {
        return gameState.getGame().getBoard().getPitList().stream()
                .collect(Collectors.toMap(Pit::getId, Pit::getNumberOfStones));
    }
}
