package lv.bol.kalaha.domain;

import lombok.Getter;
import lv.bol.kalaha.domain.pits.LargePit;
import lv.bol.kalaha.domain.pits.Pit;
import lv.bol.kalaha.domain.pits.SmallPit;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Board {

    public static final String PLAYER1_SMALL_PIT = "player1-sp";
    public static final String PLAYER2_SMALL_PIT = "player2-sp";
    public static final String PLAYER_ONE_LARGE_PIT = "player1-lp";
    public static final String PLAYER_TWO_LARGE_PIT = "player2-lp";
    @Getter
    private List<Pit> pitList;

    public Board() {
        List<Pit> list = new LinkedList<>(Arrays.asList(
                new SmallPit(PLAYER1_SMALL_PIT + "1"),
                new SmallPit(PLAYER1_SMALL_PIT + "2"),
                new SmallPit(PLAYER1_SMALL_PIT + "3"),
                new SmallPit(PLAYER1_SMALL_PIT + "4"),
                new SmallPit(PLAYER1_SMALL_PIT + "5"),
                new SmallPit(PLAYER1_SMALL_PIT + "6"),
                new LargePit(PLAYER_ONE_LARGE_PIT),
                new SmallPit(PLAYER2_SMALL_PIT + "6"),
                new SmallPit(PLAYER2_SMALL_PIT + "5"),
                new SmallPit(PLAYER2_SMALL_PIT + "4"),
                new SmallPit(PLAYER2_SMALL_PIT + "3"),
                new SmallPit(PLAYER2_SMALL_PIT + "2"),
                new SmallPit(PLAYER2_SMALL_PIT + "1"),
                new LargePit(PLAYER_TWO_LARGE_PIT)
        ));
        pitList = Collections.unmodifiableList(list);
    }
}
