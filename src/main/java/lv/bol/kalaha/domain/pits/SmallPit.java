package lv.bol.kalaha.domain.pits;

public class SmallPit extends Pit {

    private static final int INITIAL_STONE_COUNT = 6;

    public SmallPit(String id) {
        this.id = id;
        this.numberOfStones = INITIAL_STONE_COUNT;
    }

    public int takeStones() {
        int stones = this.numberOfStones;
        this.numberOfStones = 0;
        return stones;
    }

    @Override
    public void addStones(int stones) {
        this.numberOfStones += stones;
    }
}