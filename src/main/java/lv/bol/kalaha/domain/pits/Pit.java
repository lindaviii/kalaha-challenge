package lv.bol.kalaha.domain.pits;

import lombok.Data;

@Data
public abstract class Pit {
    String id;
    int numberOfStones;

    public abstract int takeStones();
    public abstract void addStones(int stones);
}
