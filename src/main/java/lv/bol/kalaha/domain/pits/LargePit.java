package lv.bol.kalaha.domain.pits;

public class LargePit extends Pit {

    public LargePit(String id) {
        this.id = id;
        this.numberOfStones = 0;
    }

    @Override
    public int takeStones() {
        this.numberOfStones = 0;
        return 0;
    }

    @Override
    public void addStones(int stones) {
        this.numberOfStones += stones;
    }
}