package lv.bol.kalaha.domain;

import lombok.Data;


@Data
public class Game {

    public static final String PLAYER_ONE_ID = "player1";
    public static final String PLAYER_TWO_ID = "player2";

    private Player playerOne;
    private Player playerTwo;
    private Board board;

    public Game(String player1Name, String player2Name){
        this.playerOne = new Player(PLAYER_ONE_ID, player1Name);
        this.playerTwo = new Player(PLAYER_TWO_ID, player2Name);
        board = new Board();
    }
}