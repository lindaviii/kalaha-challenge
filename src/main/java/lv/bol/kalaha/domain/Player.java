package lv.bol.kalaha.domain;

import lombok.Data;

@Data
public class Player {
    private String id;
    private String name;

    public Player(String id, String name) {
        this.id = id;
        this.name = name;
    }
}