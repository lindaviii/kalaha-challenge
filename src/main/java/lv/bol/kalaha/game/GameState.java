package lv.bol.kalaha.game;

import lombok.Data;
import lv.bol.kalaha.domain.Board;
import lv.bol.kalaha.domain.Game;
import lv.bol.kalaha.domain.Player;
import lv.bol.kalaha.domain.pits.LargePit;
import lv.bol.kalaha.domain.pits.Pit;
import lv.bol.kalaha.helpers.BoardHelper;
import org.springframework.stereotype.Component;

import static lv.bol.kalaha.domain.Game.PLAYER_ONE_ID;

@Data
@Component
public class GameState {

    private Game game;
    private Player activePlayer;

    public void startNewGame(String playerOneName, String playerTwoName) {
        game = new Game(playerOneName, playerTwoName);
        activePlayer = game.getPlayerOne();
    }

    public void takeTurn(String pitId) {
        Board board = game.getBoard();
        Pit startPit = BoardHelper.getPitById(board, pitId);
        int stones = startPit.takeStones();

        Pit currentPit = startPit;
        for (int i = stones; i > 0; i--) {
            currentPit = BoardHelper.getNextPit(board, currentPit);
            if (opposingPlayersLargePit(currentPit)) {
                currentPit = BoardHelper.getNextPit(board, currentPit);
            }
            currentPit.addStones(1);
        }

        captureOponentsStones(currentPit);
        if (!(currentPit instanceof LargePit)) {
            changeActivePlayer();
        }
    }

    private boolean opposingPlayersLargePit(Pit currentPit) {
        String activePlayerId = activePlayer.getId();
        return currentPit instanceof LargePit && !currentPit.getId().contains(activePlayerId);
    }

    private void captureOponentsStones(Pit currentPit) {
        if (currentPit.getNumberOfStones() == 1) {
            Pit oppositePit = BoardHelper.getOppositePit(game.getBoard(), currentPit);
            int stonesTaken = oppositePit.takeStones();
            currentPit.addStones(stonesTaken);
        }
    }

    void changeActivePlayer() {
        activePlayer = activePlayer.getId().equals(PLAYER_ONE_ID) ? game.getPlayerTwo() : game.getPlayerOne();
    }

    public boolean isGameOver() {
        return BoardHelper.allSmallPitsEmptyForAnyPlayer(game.getBoard());
    }

    public String getWinnerName() {
        return BoardHelper.getWinnerName(game.getBoard());
    }
}