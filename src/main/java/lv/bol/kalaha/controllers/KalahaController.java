package lv.bol.kalaha.controllers;

import lombok.RequiredArgsConstructor;
import lv.bol.kalaha.services.KalahaService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequiredArgsConstructor
public class KalahaController {

    @Value("${spring.application.name}")
    String appName;

    private final KalahaService service;

    @PostMapping("/new-game")
    public String startNewGame(Model model, @RequestParam String player1Name, @RequestParam String player2Name) {
        service.startNewGame(player1Name, player2Name);
        assignAttributes(model);
        return "game";
    }

    @PostMapping("/turn")
    public String updateGame(@RequestParam String pitId, Model model, HttpServletResponse response) throws IOException {
        service.takeTurn(pitId);
        if (service.isGameOver()) {
            response.sendRedirect("/end-game");
        }
        assignAttributes(model);
        return "game";
    }

    @GetMapping("/end-game")
    public String endGame(Model model) {
        model.addAttribute("appName", appName);
        model.addAttribute("winner", service.getWinnerName());
        return "game-over";
    }

    private void assignAttributes(Model model) {
        model.addAttribute("appName", appName);
        model.addAttribute("stoneCountList", service.getStoneCounts());
        model.addAttribute("activePlayer", service.getActivePlayerName());
        boolean firstPlayerOptionsAvailable = service.firstPlayerOptionsAvailable();
        model.addAttribute("playerTwoOptionsHidden", firstPlayerOptionsAvailable);
        model.addAttribute("playerOneOptionsHidden", !firstPlayerOptionsAvailable);
    }
}
