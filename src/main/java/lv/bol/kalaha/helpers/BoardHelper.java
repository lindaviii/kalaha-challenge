package lv.bol.kalaha.helpers;

import lombok.experimental.UtilityClass;
import lv.bol.kalaha.domain.Board;
import lv.bol.kalaha.domain.pits.Pit;

import java.util.Iterator;
import java.util.List;

import static lv.bol.kalaha.domain.Board.PLAYER1_SMALL_PIT;
import static lv.bol.kalaha.domain.Board.PLAYER2_SMALL_PIT;
import static lv.bol.kalaha.domain.Board.PLAYER_ONE_LARGE_PIT;
import static lv.bol.kalaha.domain.Board.PLAYER_TWO_LARGE_PIT;
import static lv.bol.kalaha.domain.Game.PLAYER_ONE_ID;
import static lv.bol.kalaha.domain.Game.PLAYER_TWO_ID;

@UtilityClass
public class BoardHelper {

    public static final String TIED_RESULT = "Friendship! It's a tie";

    public static boolean allSmallPitsEmptyForAnyPlayer(Board board) {
        List<Pit> pitList = board.getPitList();
        return getSumOfStonesInSmallPits(pitList, PLAYER1_SMALL_PIT) == 0 ||
                getSumOfStonesInSmallPits(pitList, PLAYER2_SMALL_PIT) == 0;
    }

    private static int getSumOfStonesInSmallPits(List<Pit> pitList, String playerPitIdentifier) {
        return pitList.stream()
                .filter(pit -> pit.getId().contains(playerPitIdentifier))
                .mapToInt(Pit::getNumberOfStones)
                .sum();
    }

    public static String getWinnerName(Board board) {
        int numberOfStonesInPlayer1Pit = BoardHelper.getStoneCountByPitId(board, PLAYER_ONE_LARGE_PIT);
        int numberOfStonesInPlayer2Pit = BoardHelper.getStoneCountByPitId(board, PLAYER_TWO_LARGE_PIT);
        if (numberOfStonesInPlayer1Pit == numberOfStonesInPlayer2Pit) {
            return TIED_RESULT;
        } else if (numberOfStonesInPlayer1Pit > numberOfStonesInPlayer2Pit) {
            return PLAYER_ONE_ID;
        } else {
            return PLAYER_TWO_ID;
        }
    }

    public static Pit getNextPit(Board board, Pit pit) {
        int startIndex = board.getPitList().indexOf(pit);
        Iterator<Pit> it = board.getPitList().listIterator(startIndex+1);
        return it.hasNext() ? it.next() : board.getPitList().get(0);
    }

    public static Pit getOppositePit(Board board, Pit startingPit) {
        String[] splitIdArray = startingPit.getId().split("-");
        String fieldSpecifier = splitIdArray[1];

        return board.getPitList().stream()
                .filter(pit -> pit.getId().contains(fieldSpecifier) && !startingPit.getId().equals(pit.getId()))
                .findFirst()
                .orElse(null);
    }

    public static int getStoneCountByPitId(Board board, String pitId) {
        return getPitById(board, pitId).getNumberOfStones();
    }


    public static Pit getPitById(Board board, String id) {
        return board.getPitList().stream().filter(pit -> id.equals(pit.getId()))
                .findFirst()
                .orElse(null);
    }

    public static Pit getPitByIndex(Board board, int index) {
        return board.getPitList().get(index);
    }
}