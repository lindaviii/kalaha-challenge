# Kalaha

This is a webapp for playing Kalaha.

## Starting the application

To start the application, please run the main method in the KalahaApplication class.
You should be able to view it via any browser, pointing it to localhost port 8080.
